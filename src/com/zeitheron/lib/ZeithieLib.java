package com.zeitheron.lib;

import com.zeitheron.lib.cfg.json.entries.JSONConfigEntryBool;
import com.zeitheron.lib.cfg.json.entries.JSONConfigEntryDouble;
import com.zeitheron.lib.cfg.json.entries.JSONConfigEntryJsonable;
import com.zeitheron.lib.cfg.json.entries.JSONConfigEntryLong;
import com.zeitheron.lib.cfg.json.entries.JSONConfigEntryString;
import com.zeitheron.lib.logging.Logger;
import com.zeitheron.lib.utils.ClassLoaderWrapper;

public class ZeithieLib
{
	public static final String VERSION = "1.6.6";
	
	public static final ClassLoaderWrapper classLoader = new ClassLoaderWrapper(ZeithieLib.class.getClassLoader()).accosiate("jscfg:str", JSONConfigEntryString.class).accosiate("jscfg:long", JSONConfigEntryLong.class).accosiate("jscfg:bool", JSONConfigEntryBool.class).accosiate("jscfg:double", JSONConfigEntryDouble.class).accosiate("jscfg:json", JSONConfigEntryJsonable.class);
	public static final Logger LOGGER = new Logger("hh:mm:ss", "Main", System.out);
	
	public static String getVersion()
	{
		return VERSION;
	}
}