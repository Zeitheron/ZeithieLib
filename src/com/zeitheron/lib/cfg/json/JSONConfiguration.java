package com.zeitheron.lib.cfg.json;

import java.io.IOException;
import java.io.NotSerializableException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Supplier;
import java.util.stream.Stream;

import com.zeitheron.lib.error.JSONException;
import com.zeitheron.lib.json.JSONArray;
import com.zeitheron.lib.json.JSONTokener;
import com.zeitheron.lib.json.serapi.Jsonable;

public class JSONConfiguration implements Jsonable
{
	public ArrayList<iJSONConfigEntry> entries = new ArrayList<>();
	private boolean hasChanged;
	
	public JSONConfiguration(Path json) throws IOException, JSONException
	{
		this(json.toFile().isFile() ? Files.lines(json) : new ArrayList<String>().stream());
	}
	
	public JSONConfiguration(Stream<String> lines) throws JSONException
	{
		this(() ->
		{
			StringBuilder lnr = new StringBuilder();
			lines.forEach(ln -> lnr.append(String.valueOf(ln) + "\n"));
			return lnr.toString();
		});
	}
	
	public JSONConfiguration(Supplier<String> json) throws JSONException
	{
		this(json.get());
	}
	
	@SuppressWarnings("unchecked")
	public JSONConfiguration(String json) throws JSONException
	{
		this.entries.clear();
		if(json != null && !json.isEmpty())
			this.entries.addAll((Collection<iJSONConfigEntry>) Jsonable.deserialize(json));
	}
	
	public ArrayList<iJSONConfigEntry> listEntries()
	{
		return this.entries;
	}
	
	public iJSONConfigEntry getEntryByName(String name)
	{
		int i = 0;
		while(i < this.entries.size())
		{
			iJSONConfigEntry entry = this.entries.get(i);
			if(name.equals(entry.getName()))
			{
				return entry;
			}
			++i;
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public <M extends iJSONConfigEntry> M getEntryOfTypeByName(String name, Class<M> type)
	{
		int i = 0;
		while(i < this.entries.size())
		{
			iJSONConfigEntry entry = this.entries.get(i);
			if(type.isAssignableFrom(entry.getClass()) && name.equals(entry.getName()))
				return (M) entry;
			++i;
		}
		return null;
	}
	
	public void setEntry(String name, iJSONConfigEntry entry)
	{
		iJSONConfigEntry exentry = this.getEntryByName(name);
		if(exentry != null)
		{
			this.entries.remove(exentry);
		}
		this.entries.add(entry);
		this.hasChanged = true;
	}
	
	public iJSONConfigEntry getEntryOfTypeOrDefault(String name, Class<?> type, String desc, Object value)
	{
		Class<? extends iJSONConfigEntry> typeReg = iJSONConfigEntry.entryClassByClass(type);
		iJSONConfigEntry entry = this.getEntryOfTypeByName(name, typeReg);
		if(entry != null)
		{
			return entry;
		}
		entry = iJSONConfigEntry.entryByClassAndData(type, name, desc, value);
		this.setEntry(name, entry);
		this.hasChanged = true;
		return entry;
	}
	
	public boolean hasChanged()
	{
		return this.hasChanged;
	}
	
	public void save(OutputStream out) throws IOException
	{
		out.write(this.serialize().getBytes());
	}
	
	@Override
	public String serialize()
	{
		String ret = "[]";
		try
		{
			ret = Jsonable.serializeIterable(this.entries);
		} catch(NotSerializableException e)
		{
			e.printStackTrace();
		}
		try
		{
			ret = ((JSONArray) new JSONTokener(ret).nextValue()).toString();
		} catch(JSONException e)
		{
			e.printStackTrace();
		}
		return ret;
	}
}
