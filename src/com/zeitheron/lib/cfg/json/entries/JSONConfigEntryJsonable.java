/* Decompiled with CFR 0_123. */
package com.zeitheron.lib.cfg.json.entries;

import com.zeitheron.lib.cfg.json.iJSONConfigEntry;
import com.zeitheron.lib.json.serapi.Jsonable;
import com.zeitheron.lib.json.serapi.SerializedName;

public class JSONConfigEntryJsonable implements Jsonable, iJSONConfigEntry
{
	@SerializedName(value = "Description")
	public String desc;
	@SerializedName(value = "Name")
	public String key;
	@SerializedName(value = "Value")
	public Jsonable value;
	
	@Override
	public String getName()
	{
		return this.key;
	}
	
	@Override
	public Object getValue()
	{
		return this.value;
	}
	
	@Override
	public String getComment()
	{
		return this.desc;
	}
	
	@Override
	public void setName(String name)
	{
		this.key = name;
	}
	
	@Override
	public void setComment(String comment)
	{
		this.desc = comment;
	}
	
	@Override
	public void setValue(Object value)
	{
		this.value = (Jsonable) value;
	}
	
	public boolean equals(Object obj)
	{
		return this.equalsdef(obj);
	}
}
