package com.zeitheron.lib.cfg.json;

import java.lang.reflect.InvocationTargetException;
import java.util.Objects;

import com.zeitheron.lib.cfg.json.entries.JSONConfigEntryBool;
import com.zeitheron.lib.cfg.json.entries.JSONConfigEntryDouble;
import com.zeitheron.lib.cfg.json.entries.JSONConfigEntryJsonable;
import com.zeitheron.lib.cfg.json.entries.JSONConfigEntryLong;
import com.zeitheron.lib.cfg.json.entries.JSONConfigEntryString;
import com.zeitheron.lib.error.Errors;
import com.zeitheron.lib.json.serapi.Jsonable;

public interface iJSONConfigEntry extends Jsonable
{
	public String getName();
	
	public String getComment();
	
	public Object getValue();
	
	public void setName(String var1);
	
	public void setComment(String var1);
	
	public void setValue(Object var1);
	
	default public boolean equalsdef(Object obj)
	{
		if(!(obj instanceof iJSONConfigEntry))
		{
			return false;
		}
		iJSONConfigEntry entry = (iJSONConfigEntry) obj;
		if(!entry.getName().equals(this.getName()))
		{
			return false;
		}
		if(!entry.getComment().equals(this.getComment()))
		{
			return false;
		}
		if(!Objects.equals(entry.getValue(), this.getValue()))
		{
			return false;
		}
		return true;
	}
	
	public static iJSONConfigEntry entryByClassAndData(Class<?> type, String name, String desc, Object value)
	{
		iJSONConfigEntry s = iJSONConfigEntry.entryByClass(type);
		s.setName(name);
		s.setComment(desc);
		s.setValue(value);
		return s;
	}
	
	public static Class<? extends iJSONConfigEntry> entryClassByClass(Class<?> type)
	{
		if(Integer.TYPE.isAssignableFrom(type) || Integer.class.isAssignableFrom(type) || Long.TYPE.isAssignableFrom(type) || Long.class.isAssignableFrom(type) || Short.TYPE.isAssignableFrom(type) || Short.class.isAssignableFrom(type) || Byte.TYPE.isAssignableFrom(type) || Byte.class.isAssignableFrom(type))
		{
			return JSONConfigEntryLong.class;
		}
		if(Boolean.TYPE.isAssignableFrom(type) || Boolean.class.isAssignableFrom(type))
		{
			return JSONConfigEntryBool.class;
		}
		if(Double.TYPE.isAssignableFrom(type) || Double.class.isAssignableFrom(type) || Float.TYPE.isAssignableFrom(type) || Float.class.isAssignableFrom(type))
		{
			return JSONConfigEntryDouble.class;
		}
		if(Jsonable.class.isAssignableFrom(type))
		{
			return JSONConfigEntryJsonable.class;
		}
		if(String.class.isAssignableFrom(type))
		{
			return JSONConfigEntryString.class;
		}
		return null;
	}
	
	public static iJSONConfigEntry entryByClass(Class<?> type)
	{
		try
		{
			return iJSONConfigEntry.entryClassByClass(type).getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
		} catch(IllegalAccessException | IllegalArgumentException | InstantiationException | NoSuchMethodException | SecurityException | InvocationTargetException e)
		{
			Errors.propagate(e);
			return null;
		}
	}
}
