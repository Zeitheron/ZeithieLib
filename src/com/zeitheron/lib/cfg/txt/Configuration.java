package com.zeitheron.lib.cfg.txt;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;

import com.zeitheron.lib.utils.BufferedReaderUtils;
import com.zeitheron.lib.utils.iIntent;

public class Configuration implements iIntent
{
	private Map<String, Object> values = new HashMap<String, Object>();
	private Map<String, String> comments = new HashMap<String, String>();
	private final File src;
	private String lastComment = "";
	
	public Configuration(File from)
	{
		this.src = from;
		this.load();
	}
	
	@SuppressWarnings("unchecked")
	public <T> T getOrSetDef(String key, T fallback, String comment)
	{
		Object located = this.values.get(key);
		this.comments.put(key, comment);
		if(located != null && fallback.getClass().isAssignableFrom(located.getClass()))
			return (T) located;
		this.values.put(key, fallback);
		return fallback;
	}
	
	public void load()
	{
		if(!this.src.exists())
		{
			return;
		}
		try(BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(this.src), "UTF-8")))
		{
			BufferedReaderUtils.forEachLine(br, this);
		} catch(Throwable err)
		{
			err.printStackTrace();
		}
		this.lastComment = "";
	}
	
	public void save()
	{
		try(BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.src), "UTF-8")))
		{
			bw.write(this.toString());
		} catch(Throwable bw)
		{
		}
	}
	
	@Override
	public void run(int code, Object... data)
	{
		if(code == 0)
		{
			String ln = data[0].toString();
			if(ln.startsWith("#"))
				this.lastComment = String.valueOf(this.lastComment) + ln.substring(1) + "\n";
			if(ln.startsWith("#") || !ln.contains("="))
				return;
			String key = ln.substring(0, ln.indexOf("="));
			String val = ln.substring(ln.indexOf("=") + 1);
			if(this.lastComment.endsWith("\n"))
				this.lastComment = this.lastComment.substring(0, this.lastComment.length() - 1);
			this.comments.put(key, this.lastComment);
			this.lastComment = "";
			Long asLong = null;
			Double asDouble = null;
			String asString = null;
			Boolean asBool = null;
			try
			{
				asLong = Long.parseLong(val);
				this.values.put(key, asLong);
				return;
			} catch(Throwable throwable)
			{
				if(asLong == null)
				{
					try
					{
						asDouble = Double.parseDouble(val);
						this.values.put(key, asDouble);
						return;
					} catch(Throwable throwable2)
					{
						// empty catch block
					}
				}
				if(asDouble == null)
				{
					try
					{
						asBool = Boolean.parseBoolean(val);
						this.values.put(key, asBool);
						return;
					} catch(Throwable throwable3)
					{
						// empty catch block
					}
				}
				if(asBool != null)
					return;
				asString = val;
				this.values.put(key, asString);
				return;
			}
		}
	}
	
	public String toString()
	{
		String s = "";
		for(String key : this.values.keySet())
		{
			String val = this.values.get(key) + "";
			String comment = this.comments.get(key);
			if(comment != null && comment.length() > 0)
			{
				String[] arrstring = comment.split("\n");
				int n = arrstring.length;
				int n2 = 0;
				while(n2 < n)
				{
					String s2 = arrstring[n2];
					s = String.valueOf(s) + "#" + s2 + "\n";
					++n2;
				}
			}
			s = String.valueOf(s) + key + "=" + val + "\n\n";
		}
		if(s.endsWith("\n\n"))
		{
			s = s.substring(0, s.length() - 2);
		}
		return s;
	}
}
