/* Decompiled with CFR 0_123. */
package com.zeitheron.lib.database;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import com.zeitheron.lib.database.Database;
import com.zeitheron.lib.database.DatabaseEntry;
import com.zeitheron.lib.database.EnumDBType;
import com.zeitheron.lib.error.DatabaseException;
import com.zeitheron.lib.tuple.TwoTuple;

public class DatabaseWrapper
{
	public final Database db;
	
	public DatabaseWrapper(Database db)
	{
		this.db = db;
	}
	
	public int size()
	{
		return this.db.size();
	}
	
	public TwoTuple<DatabaseEntry, Integer> addEntry(String content, String code) throws DatabaseException
	{
		if(this.db.dbType == EnumDBType.KEY_VALUE)
		{
			DatabaseEntry dbe = new DatabaseEntry(content.getBytes(), code.getBytes(), this.db.dbType);
			return new TwoTuple<DatabaseEntry, Integer>(dbe, this.db.addEntry(dbe));
		}
		return null;
	}
	
	public TwoTuple<DatabaseEntry, Integer> addEntry(String code) throws DatabaseException
	{
		if(this.db.dbType == EnumDBType.MATCHER)
		{
			DatabaseEntry dbe = new DatabaseEntry(code.getBytes(), this.db.dbType);
			return new TwoTuple<DatabaseEntry, Integer>(dbe, this.db.addEntry(dbe));
		}
		return null;
	}
	
	public DatabaseEntry getEntry(int id)
	{
		return this.db.getEntry(id);
	}
	
	public void save(File out) throws IOException
	{
		FileOutputStream fos = new FileOutputStream(out);
		this.save(fos);
		fos.close();
	}
	
	public void save(OutputStream out) throws IOException
	{
		this.db.save(out);
	}
}
