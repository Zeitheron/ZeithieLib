/* Decompiled with CFR 0_123. */
package com.zeitheron.lib.error;

public class DatabaseException extends Exception
{
	private static final long serialVersionUID = 4376838344934151609L;
	
	public DatabaseException(String cause)
	{
		super(cause);
	}
}
