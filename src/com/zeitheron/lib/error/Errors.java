/* Decompiled with CFR 0_123. */
package com.zeitheron.lib.error;

import com.zeitheron.lib.error.WrappedError;

public class Errors
{
	public static void propagate(Throwable err)
	{
		throw new WrappedError(err);
	}
}
