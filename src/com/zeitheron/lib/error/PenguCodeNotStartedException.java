package com.zeitheron.lib.error;

public class PenguCodeNotStartedException extends Error
{
	private static final long serialVersionUID = -6974133987545848328L;

	public PenguCodeNotStartedException()
	{
		super("Library PenguCode is not yet started. Please call PenguCode.start();");
	}
}
