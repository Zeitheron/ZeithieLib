package com.zeitheron.lib.events;

public abstract class Event
{
	protected boolean isCanceled;
	
	public boolean isCancelable()
	{
		Cancelable c;
		return (c = getClass().getDeclaredAnnotation(Cancelable.class)) != null && c.value();
	}
	
	public void setCanceled(boolean isCanceled)
	{
		if(isCanceled)
			if(isCancelable())
				this.isCanceled = true;
			else
				throw new RuntimeException("Unable to cancel event " + getClass().getName() + ": It's not cancelable!");
		else
			this.isCanceled = false;
	}
	
	public boolean isCanceled()
	{
		return isCanceled;
	}
}