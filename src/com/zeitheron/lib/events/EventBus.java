package com.zeitheron.lib.events;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import com.zeitheron.lib.utils.PriorityList;

public class EventBus
{
	private PriorityList<EventNode> nodes = new PriorityList<>();
	IBusErrorHandler error = null;
	
	public void setErrorHandler(IBusErrorHandler error)
	{
		this.error = error;
	}
	
	{
		nodes.setPriority(i -> i.getPriority());
	}
	
	public static List<EventNode> nodes(Object listener)
	{
		List<EventNode> nodes = new ArrayList<>();
		for(Method m : listener.getClass().getDeclaredMethods())
			if(m.getAnnotation(EventHandler.class) != null)
				nodes.add(new EventNode(m, listener));
		return nodes;
	}
	
	public List<EventNode> register(Object subscriber)
	{
		List<EventNode> nd = nodes(subscriber);
		nd.forEach(node -> nodes.add(node));
		return nd;
	}
	
	public EventState dispatch(Event event)
	{
		/** Java-8 implementation */
		nodes.stream().filter(node -> node.canHandle(event)).forEach(node ->
		{
			if(!event.isCanceled || node.receiveCanceled)
				node.handle(event, this);
		});
		
		return event.isCanceled ? EventState.CANCELED : EventState.ALIVE;
	}
}