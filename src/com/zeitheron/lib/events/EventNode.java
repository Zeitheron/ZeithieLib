package com.zeitheron.lib.events;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class EventNode
{
	private Method t;
	public Consumer<Event> pre, post;
	private int prio;
	private Object owner;
	private Predicate<Class<? extends Event>> acceptance;
	boolean receiveCanceled;
	
	public EventNode(Method tar, Object owner)
	{
		if(tar.getParameterTypes().length != 1)
			throw new RuntimeException("Failed to create event node for " + owner.getClass().getName() + "." + tar.getName() + ": Argument count doesn't match");
		
		Class<?> evh;
		
		if(!Event.class.isAssignableFrom(evh = tar.getParameterTypes()[0]))
			throw new RuntimeException("The first argument is not an event acceptor for " + owner.getClass().getName() + "." + tar.getName() + "!");
		
		EventHandler eh;
		if((eh = tar.getAnnotation(EventHandler.class)) == null)
		{
			prio = 0;
			receiveCanceled = false;
		} else
		{
			prio = eh.priority();
			receiveCanceled = eh.receiveCanceled();
		}
		
		tar.setAccessible(true);
		
		this.t = tar;
		this.owner = owner;
		this.acceptance = c -> evh.isAssignableFrom(c);
	}
	
	public int getPriority()
	{
		return prio;
	}
	
	public boolean canHandle(Event e)
	{
		return acceptance.test(e.getClass());
	}
	
	public void handle(Event e, EventBus bus)
	{
		try
		{
			if(pre != null)
				pre.accept(e);
			this.t.invoke(this.owner, e);
			if(post != null)
				post.accept(e);
		} catch(InvocationTargetException e1)
		{
			if(bus.error != null)
				bus.error.handleError(bus, this.owner, e, e1.getCause() != null ? e1.getCause() : e1);
			else
				throw new RuntimeException(e1);
		} catch(Throwable e1)
		{
			if(bus.error != null)
				bus.error.handleError(bus, this.owner, e, e1);
			else
				throw new RuntimeException(e1);
		}
	}
}