package com.zeitheron.lib.events;

public enum EventState
{
	CANCELED,
	ALIVE;
}