package com.zeitheron.lib.events;

public interface IBusErrorHandler
{
	void handleError(EventBus bus, Object handled, Event event, Throwable err);
}