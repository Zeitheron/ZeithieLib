package com.zeitheron.lib.io;

import java.io.IOException;
import java.io.OutputStream;

public class VoidOutputStream extends OutputStream
{
	@Override
	public void write(int b) throws IOException
	{
	}
}