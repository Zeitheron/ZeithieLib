package com.zeitheron.lib.io.cache;

import java.io.InputStream;
import java.io.OutputStream;

import com.zeitheron.lib.io.VoidInputStream;
import com.zeitheron.lib.io.VoidOutputStream;

public final class VoidCacher implements iCacher
{
	@Override
	public OutputStream put(String url)
	{
		return new VoidOutputStream();
	}
	
	@Override
	public InputStream pull(String url)
	{
		return new VoidInputStream();
	}
	
	@Override
	public boolean isActuallyWorking()
	{
		return true;
	}
}