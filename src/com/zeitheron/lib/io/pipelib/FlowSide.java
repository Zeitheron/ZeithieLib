package com.zeitheron.lib.io.pipelib;

public enum FlowSide
{
	CLIENT,
	SERVER;
	
	public boolean isServer()
	{
		return this == SERVER;
	}
	
	public boolean isClient()
	{
		return this == CLIENT;
	}
}