package com.zeitheron.lib.io.pipelib;

public interface iFlowListener
{
	PipeFlow onFlow(FlowSide target, PipeFlow flow);
}