package com.zeitheron.lib.logging;

public enum LogLevel
{
	INFO, WARN, ERR, FATAL, FINE, FINER, FINEST, DEBUG;
}