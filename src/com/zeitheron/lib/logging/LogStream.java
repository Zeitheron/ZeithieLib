package com.zeitheron.lib.logging;

import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.EnumMap;
import java.util.Locale;

public class LogStream
{
	public final PrintStream writer;
	public final DateFormat dateFormat;
	public final String name;
	public final ThreadLocal<LogLevel> lvl = ThreadLocal.withInitial(() -> LogLevel.INFO);
	public final EnumMap<LogLevel, Boolean> disabled = new EnumMap<>(LogLevel.class);
	
	public void enable(LogLevel l)
	{
		this.disabled.put(l, false);
	}
	
	public void disable(LogLevel l)
	{
		this.disabled.put(l, true);
	}
	
	LogStream(String date, String name, PrintStream writer)
	{
		this.dateFormat = new SimpleDateFormat(date);
		this.name = name;
		this.writer = writer;
	}
	
	public String getDatePrefix()
	{
		return "[" + this.dateFormat.format(Calendar.getInstance().getTime()) + "]";
	}
	
	public String getThreadLevelPrefix(LogLevel l)
	{
		return "[" + l.name().toLowerCase(Locale.ENGLISH) + " in \"" + Thread.currentThread().getName() + "\"]";
	}
	
	public String getNamePrefix()
	{
		return "[" + this.name + "]";
	}
	
	public void switchTo(LogLevel l)
	{
		this.lvl.set(l);
	}
	
	public /* varargs */ void log(String format, boolean nln, Object... args)
	{
		if(this.disabled.getOrDefault((Object) this.lvl.get(), false) == Boolean.TRUE)
		{
			return;
		}
		format = String.format(format, args);
		this.writer.print(String.valueOf(this.getDatePrefix()) + " " + this.getThreadLevelPrefix(this.lvl.get()) + " " + this.getNamePrefix() + " " + format);
		if(nln)
		{
			this.writer.append('\n');
		}
	}
}
