/* Decompiled with CFR 0_123. */
package com.zeitheron.lib.logging;

import java.io.PrintStream;

import com.zeitheron.lib.logging.LogLevel;
import com.zeitheron.lib.logging.LogStream;

public class Logger
{
	public final LogStream[] streams;
	
	public /* varargs */ Logger(String date, String name, PrintStream... writers)
	{
		this.streams = new LogStream[writers.length];
		int i = 0;
		while(i < this.streams.length)
		{
			this.streams[i] = new LogStream(date, name, writers[i]);
			++i;
		}
	}
	
	public void enable(LogLevel l)
	{
		LogStream[] arrlogStream = this.streams;
		int n = arrlogStream.length;
		int n2 = 0;
		while(n2 < n)
		{
			LogStream ls = arrlogStream[n2];
			ls.enable(l);
			++n2;
		}
	}
	
	public void disable(LogLevel l)
	{
		LogStream[] arrlogStream = this.streams;
		int n = arrlogStream.length;
		int n2 = 0;
		while(n2 < n)
		{
			LogStream ls = arrlogStream[n2];
			ls.disable(l);
			++n2;
		}
	}
	
	public /* varargs */ void info(String format, Object... args)
	{
		LogStream[] arrlogStream = this.streams;
		int n = arrlogStream.length;
		int n2 = 0;
		while(n2 < n)
		{
			LogStream ls = arrlogStream[n2];
			ls.switchTo(LogLevel.INFO);
			ls.log(format, true, args);
			++n2;
		}
	}
	
	public /* varargs */ void warn(String format, Object... args)
	{
		LogStream[] arrlogStream = this.streams;
		int n = arrlogStream.length;
		int n2 = 0;
		while(n2 < n)
		{
			LogStream ls = arrlogStream[n2];
			ls.switchTo(LogLevel.WARN);
			ls.log(format, true, args);
			++n2;
		}
	}
	
	public /* varargs */ void err(String format, Object... args)
	{
		LogStream[] arrlogStream = this.streams;
		int n = arrlogStream.length;
		int n2 = 0;
		while(n2 < n)
		{
			LogStream ls = arrlogStream[n2];
			ls.switchTo(LogLevel.ERR);
			ls.log(format, true, args);
			++n2;
		}
	}
	
	public /* varargs */ void fatal(String format, Object... args)
	{
		LogStream[] arrlogStream = this.streams;
		int n = arrlogStream.length;
		int n2 = 0;
		while(n2 < n)
		{
			LogStream ls = arrlogStream[n2];
			ls.switchTo(LogLevel.FATAL);
			ls.log(format, true, args);
			++n2;
		}
	}
	
	public /* varargs */ void fine(String format, Object... args)
	{
		LogStream[] arrlogStream = this.streams;
		int n = arrlogStream.length;
		int n2 = 0;
		while(n2 < n)
		{
			LogStream ls = arrlogStream[n2];
			ls.switchTo(LogLevel.FINE);
			ls.log(format, true, args);
			++n2;
		}
	}
	
	public /* varargs */ void finer(String format, Object... args)
	{
		LogStream[] arrlogStream = this.streams;
		int n = arrlogStream.length;
		int n2 = 0;
		while(n2 < n)
		{
			LogStream ls = arrlogStream[n2];
			ls.switchTo(LogLevel.FINER);
			ls.log(format, true, args);
			++n2;
		}
	}
	
	public /* varargs */ void finest(String format, Object... args)
	{
		LogStream[] arrlogStream = this.streams;
		int n = arrlogStream.length;
		int n2 = 0;
		while(n2 < n)
		{
			LogStream ls = arrlogStream[n2];
			ls.switchTo(LogLevel.FINEST);
			ls.log(format, true, args);
			++n2;
		}
	}
	
	public /* varargs */ void debug(String format, Object... args)
	{
		LogStream[] arrlogStream = this.streams;
		int n = arrlogStream.length;
		int n2 = 0;
		while(n2 < n)
		{
			LogStream ls = arrlogStream[n2];
			ls.switchTo(LogLevel.DEBUG);
			ls.log(format, true, args);
			++n2;
		}
	}
}
