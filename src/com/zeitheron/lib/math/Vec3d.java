/* Decompiled with CFR 0_123. */
package com.zeitheron.lib.math;

import com.zeitheron.lib.math.MathHelper;
import com.zeitheron.lib.math.Vec3i;

public class Vec3d
{
	public static final Vec3d ZERO = new Vec3d(0.0, 0.0, 0.0);
	public final double x;
	public final double y;
	public final double z;
	
	public Vec3d(double x, double y, double z)
	{
		if(x == -0.0)
		{
			x = 0.0;
		}
		if(y == -0.0)
		{
			y = 0.0;
		}
		if(z == -0.0)
		{
			z = 0.0;
		}
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public Vec3d(Vec3i vector)
	{
		this(vector.getX(), vector.getY(), vector.getZ());
	}
	
	public Vec3d subtractReverse(Vec3d vec)
	{
		return new Vec3d(vec.x - this.x, vec.y - this.y, vec.z - this.z);
	}
	
	public Vec3d normalize()
	{
		double d0 = MathHelper.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
		return d0 < 1.0E-4 ? ZERO : new Vec3d(this.x / d0, this.y / d0, this.z / d0);
	}
	
	public double dotProduct(Vec3d vec)
	{
		return this.x * vec.x + this.y * vec.y + this.z * vec.z;
	}
	
	public Vec3d crossProduct(Vec3d vec)
	{
		return new Vec3d(this.y * vec.z - this.z * vec.y, this.z * vec.x - this.x * vec.z, this.x * vec.y - this.y * vec.x);
	}
	
	public Vec3d subtract(Vec3d vec)
	{
		return this.subtract(vec.x, vec.y, vec.z);
	}
	
	public Vec3d subtract(double x, double y, double z)
	{
		return this.addVector(-x, -y, -z);
	}
	
	public Vec3d add(Vec3d vec)
	{
		return this.addVector(vec.x, vec.y, vec.z);
	}
	
	public Vec3d addVector(double x, double y, double z)
	{
		return new Vec3d(this.x + x, this.y + y, this.z + z);
	}
	
	public double distanceTo(Vec3d vec)
	{
		double d0 = vec.x - this.x;
		double d1 = vec.y - this.y;
		double d2 = vec.z - this.z;
		return MathHelper.sqrt(d0 * d0 + d1 * d1 + d2 * d2);
	}
	
	public double squareDistanceTo(Vec3d vec)
	{
		double d0 = vec.x - this.x;
		double d1 = vec.y - this.y;
		double d2 = vec.z - this.z;
		return d0 * d0 + d1 * d1 + d2 * d2;
	}
	
	public double squareDistanceTo(double xIn, double yIn, double zIn)
	{
		double d0 = xIn - this.x;
		double d1 = yIn - this.y;
		double d2 = zIn - this.z;
		return d0 * d0 + d1 * d1 + d2 * d2;
	}
	
	public Vec3d scale(double p_186678_1_)
	{
		return new Vec3d(this.x * p_186678_1_, this.y * p_186678_1_, this.z * p_186678_1_);
	}
	
	public double lengthVector()
	{
		return MathHelper.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
	}
	
	public double lengthSquared()
	{
		return this.x * this.x + this.y * this.y + this.z * this.z;
	}
	
	public Vec3d getIntermediateWithXValue(Vec3d vec, double x)
	{
		double d0 = vec.x - this.x;
		double d1 = vec.y - this.y;
		double d2 = vec.z - this.z;
		if(d0 * d0 < 1.0000000116860974E-7)
		{
			return null;
		}
		double d3 = (x - this.x) / d0;
		return d3 >= 0.0 && d3 <= 1.0 ? new Vec3d(this.x + d0 * d3, this.y + d1 * d3, this.z + d2 * d3) : null;
	}
	
	public Vec3d getIntermediateWithYValue(Vec3d vec, double y)
	{
		double d0 = vec.x - this.x;
		double d1 = vec.y - this.y;
		double d2 = vec.z - this.z;
		if(d1 * d1 < 1.0000000116860974E-7)
		{
			return null;
		}
		double d3 = (y - this.y) / d1;
		return d3 >= 0.0 && d3 <= 1.0 ? new Vec3d(this.x + d0 * d3, this.y + d1 * d3, this.z + d2 * d3) : null;
	}
	
	public Vec3d getIntermediateWithZValue(Vec3d vec, double z)
	{
		double d0 = vec.x - this.x;
		double d1 = vec.y - this.y;
		double d2 = vec.z - this.z;
		if(d2 * d2 < 1.0000000116860974E-7)
		{
			return null;
		}
		double d3 = (z - this.z) / d2;
		return d3 >= 0.0 && d3 <= 1.0 ? new Vec3d(this.x + d0 * d3, this.y + d1 * d3, this.z + d2 * d3) : null;
	}
	
	public boolean equals(Object p_equals_1_)
	{
		if(this == p_equals_1_)
		{
			return true;
		}
		if(!(p_equals_1_ instanceof Vec3d))
		{
			return false;
		}
		Vec3d Vec3D = (Vec3d) p_equals_1_;
		return Double.compare(Vec3D.x, this.x) != 0 ? false : (Double.compare(Vec3D.y, this.y) != 0 ? false : Double.compare(Vec3D.z, this.z) == 0);
	}
	
	public int hashCode()
	{
		long j = Double.doubleToLongBits(this.x);
		int i = (int) (j ^ j >>> 32);
		j = Double.doubleToLongBits(this.y);
		i = 31 * i + (int) (j ^ j >>> 32);
		j = Double.doubleToLongBits(this.z);
		i = 31 * i + (int) (j ^ j >>> 32);
		return i;
	}
	
	public String toString()
	{
		return "(" + this.x + ", " + this.y + ", " + this.z + ")";
	}
	
	public Vec3d rotatePitch(float pitch)
	{
		float f = MathHelper.cos(pitch);
		float f1 = MathHelper.sin(pitch);
		double d0 = this.x;
		double d1 = this.y * (double) f + this.z * (double) f1;
		double d2 = this.z * (double) f - this.y * (double) f1;
		return new Vec3d(d0, d1, d2);
	}
	
	public Vec3d rotateYaw(float yaw)
	{
		float f = MathHelper.cos(yaw);
		float f1 = MathHelper.sin(yaw);
		double d0 = this.x * (double) f + this.z * (double) f1;
		double d1 = this.y;
		double d2 = this.z * (double) f - this.x * (double) f1;
		return new Vec3d(d0, d1, d2);
	}
	
	public static Vec3d fromPitchYaw(float p_189986_0_, float p_189986_1_)
	{
		float f = MathHelper.cos((-p_189986_1_) * 0.017453292f - 3.1415927f);
		float f1 = MathHelper.sin((-p_189986_1_) * 0.017453292f - 3.1415927f);
		float f2 = -MathHelper.cos((-p_189986_0_) * 0.017453292f);
		float f3 = MathHelper.sin((-p_189986_0_) * 0.017453292f);
		return new Vec3d(f1 * f2, f3, f * f2);
	}
}
