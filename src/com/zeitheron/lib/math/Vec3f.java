/* Decompiled with CFR 0_123. */
package com.zeitheron.lib.math;

public class Vec3f
{
	public float x;
	public float y;
	public float z;
	
	public Vec3f(float x, float y, float z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public Vec3f interpolateTo(Vec3f t, float p)
	{
		float xt = this.x + (t.x - this.x) * p;
		float yt = this.y + (t.y - this.y) * p;
		float zt = this.z + (t.z - this.z) * p;
		return new Vec3f(xt, yt, zt);
	}
	
	public void set(float x, float y, float z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}
}
