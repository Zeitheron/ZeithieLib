package com.zeitheron.lib.shaded.nbt;

public class NBTSizeTracker
{
	public static final NBTSizeTracker INFINITE = new NBTSizeTracker(0)
	{
		@Override
		public void read(long bits)
		{
		}
	};
	private final long max;
	private long read;
	
	public NBTSizeTracker(long max)
	{
		this.max = max;
	}
	
	public void read(long bits)
	{
		this.read += bits / 8;
		if(this.read > this.max)
		{
			throw new RuntimeException("Tried to read NBT tag that was too big; tried to allocate: " + this.read + "bytes where max allowed: " + this.max);
		}
	}
	
	public static void readUTF(NBTSizeTracker tracker, String data)
	{
		tracker.read(16);
		if(data == null)
		{
			return;
		}
		int len = data.length();
		int utflen = 0;
		int i = 0;
		while(i < len)
		{
			char c = data.charAt(i);
			utflen = c >= '\u0001' && c <= '' ? ++utflen : (c > '\u07ff' ? (utflen += 3) : (utflen += 2));
			++i;
		}
		tracker.read(8 * utflen);
	}
	
}
