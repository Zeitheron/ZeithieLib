package com.zeitheron.lib.shaded.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.zeitheron.lib.shaded.nbt.NBTBase;
import com.zeitheron.lib.shaded.nbt.NBTSizeTracker;

@SuppressWarnings("serial")
public class NBTTagByteArray extends NBTBase
{
	private byte[] data;
	
	NBTTagByteArray()
	{
	}
	
	public NBTTagByteArray(byte[] data)
	{
		this.data = data;
	}
	
	public NBTTagByteArray(List<Byte> p_i47529_1_)
	{
		this(NBTTagByteArray.toArray(p_i47529_1_));
	}
	
	private static byte[] toArray(List<Byte> p_193589_0_)
	{
		byte[] abyte = new byte[p_193589_0_.size()];
		int i = 0;
		while(i < p_193589_0_.size())
		{
			Byte obyte = p_193589_0_.get(i);
			abyte[i] = obyte == null ? 0 : obyte.byteValue();
			++i;
		}
		return abyte;
	}
	
	@Override
	void write(DataOutput output) throws IOException
	{
		output.writeInt(this.data.length);
		output.write(this.data);
	}
	
	@Override
	void read(DataInput input, int depth, NBTSizeTracker sizeTracker) throws IOException
	{
		sizeTracker.read(192);
		int i = input.readInt();
		sizeTracker.read(8 * i);
		this.data = new byte[i];
		input.readFully(this.data);
	}
	
	@Override
	public byte getId()
	{
		return 7;
	}
	
	@Override
	public String toString()
	{
		StringBuilder stringbuilder = new StringBuilder("[B;");
		int i = 0;
		while(i < this.data.length)
		{
			if(i != 0)
			{
				stringbuilder.append(',');
			}
			stringbuilder.append(this.data[i]).append('B');
			++i;
		}
		return stringbuilder.append(']').toString();
	}
	
	@Override
	public NBTBase copy()
	{
		byte[] abyte = new byte[this.data.length];
		System.arraycopy(this.data, 0, abyte, 0, this.data.length);
		return new NBTTagByteArray(abyte);
	}
	
	@Override
	public boolean equals(Object p_equals_1_)
	{
		if(super.equals(p_equals_1_) && Arrays.equals(this.data, ((NBTTagByteArray) p_equals_1_).data))
		{
			return true;
		}
		return false;
	}
	
	@Override
	public int hashCode()
	{
		return super.hashCode() ^ Arrays.hashCode(this.data);
	}
	
	public byte[] getByteArray()
	{
		return this.data;
	}
}
