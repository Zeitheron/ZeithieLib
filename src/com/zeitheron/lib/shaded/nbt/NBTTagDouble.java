package com.zeitheron.lib.shaded.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.zeitheron.lib.math.MathHelper;

@SuppressWarnings("serial")
public class NBTTagDouble extends NBTPrimitive
{
	private double data;
	
	NBTTagDouble()
	{
	}
	
	public NBTTagDouble(double data)
	{
		this.data = data;
	}
	
	@Override
	void write(DataOutput output) throws IOException
	{
		output.writeDouble(this.data);
	}
	
	@Override
	void read(DataInput input, int depth, NBTSizeTracker sizeTracker) throws IOException
	{
		sizeTracker.read(128);
		this.data = input.readDouble();
	}
	
	@Override
	public byte getId()
	{
		return 6;
	}
	
	@Override
	public String toString()
	{
		return String.valueOf(this.data) + "d";
	}
	
	@Override
	public NBTTagDouble copy()
	{
		return new NBTTagDouble(this.data);
	}
	
	@Override
	public boolean equals(Object p_equals_1_)
	{
		if(super.equals(p_equals_1_) && this.data == ((NBTTagDouble) p_equals_1_).data)
		{
			return true;
		}
		return false;
	}
	
	@Override
	public int hashCode()
	{
		long i = Double.doubleToLongBits(this.data);
		return super.hashCode() ^ (int) (i ^ i >>> 32);
	}
	
	@Override
	public long getLong()
	{
		return (long) Math.floor(this.data);
	}
	
	@Override
	public int getInt()
	{
		return MathHelper.floor((float) this.data);
	}
	
	@Override
	public short getShort()
	{
		return (short) (MathHelper.floor(this.data) & 65535);
	}
	
	@Override
	public byte getByte()
	{
		return (byte) (MathHelper.floor(this.data) & 255);
	}
	
	@Override
	public double getDouble()
	{
		return this.data;
	}
	
	@Override
	public float getFloat()
	{
		return (float) this.data;
	}
}
