package com.zeitheron.lib.shaded.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.zeitheron.lib.math.MathHelper;

@SuppressWarnings("serial")
public class NBTTagFloat extends NBTPrimitive
{
	private float data;
	
	NBTTagFloat()
	{
	}
	
	public NBTTagFloat(float data)
	{
		this.data = data;
	}
	
	@Override
	void write(DataOutput output) throws IOException
	{
		output.writeFloat(this.data);
	}
	
	@Override
	void read(DataInput input, int depth, NBTSizeTracker sizeTracker) throws IOException
	{
		sizeTracker.read(96);
		this.data = input.readFloat();
	}
	
	@Override
	public byte getId()
	{
		return 5;
	}
	
	@Override
	public String toString()
	{
		return String.valueOf(this.data) + "f";
	}
	
	@Override
	public NBTTagFloat copy()
	{
		return new NBTTagFloat(this.data);
	}
	
	@Override
	public boolean equals(Object p_equals_1_)
	{
		if(super.equals(p_equals_1_) && this.data == ((NBTTagFloat) p_equals_1_).data)
		{
			return true;
		}
		return false;
	}
	
	@Override
	public int hashCode()
	{
		return super.hashCode() ^ Float.floatToIntBits(this.data);
	}
	
	@Override
	public long getLong()
	{
		return (long) this.data;
	}
	
	@Override
	public int getInt()
	{
		return MathHelper.floor(this.data);
	}
	
	@Override
	public short getShort()
	{
		return (short) (MathHelper.floor(this.data) & 65535);
	}
	
	@Override
	public byte getByte()
	{
		return (byte) (MathHelper.floor(this.data) & 255);
	}
	
	@Override
	public double getDouble()
	{
		return this.data;
	}
	
	@Override
	public float getFloat()
	{
		return this.data;
	}
}
