package com.zeitheron.lib.shaded.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

@SuppressWarnings("serial")
public class NBTTagLong extends NBTPrimitive
{
	private long data;
	
	NBTTagLong()
	{
	}
	
	public NBTTagLong(long data)
	{
		this.data = data;
	}
	
	@Override
	void write(DataOutput output) throws IOException
	{
		output.writeLong(this.data);
	}
	
	@Override
	void read(DataInput input, int depth, NBTSizeTracker sizeTracker) throws IOException
	{
		sizeTracker.read(128);
		this.data = input.readLong();
	}
	
	@Override
	public byte getId()
	{
		return 4;
	}
	
	@Override
	public String toString()
	{
		return String.valueOf(this.data) + "L";
	}
	
	@Override
	public NBTTagLong copy()
	{
		return new NBTTagLong(this.data);
	}
	
	@Override
	public boolean equals(Object p_equals_1_)
	{
		if(super.equals(p_equals_1_) && this.data == ((NBTTagLong) p_equals_1_).data)
		{
			return true;
		}
		return false;
	}
	
	@Override
	public int hashCode()
	{
		return super.hashCode() ^ (int) (this.data ^ this.data >>> 32);
	}
	
	@Override
	public long getLong()
	{
		return this.data;
	}
	
	@Override
	public int getInt()
	{
		return (int) (this.data & -1);
	}
	
	@Override
	public short getShort()
	{
		return (short) (this.data & 65535);
	}
	
	@Override
	public byte getByte()
	{
		return (byte) (this.data & 255);
	}
	
	@Override
	public double getDouble()
	{
		return this.data;
	}
	
	@Override
	public float getFloat()
	{
		return this.data;
	}
}
