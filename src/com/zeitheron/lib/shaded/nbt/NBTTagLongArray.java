package com.zeitheron.lib.shaded.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@SuppressWarnings("serial")
public class NBTTagLongArray extends NBTBase
{
	private long[] data;
	
	NBTTagLongArray()
	{
	}
	
	public NBTTagLongArray(long[] p_i47524_1_)
	{
		this.data = p_i47524_1_;
	}
	
	public NBTTagLongArray(List<Long> p_i47525_1_)
	{
		this(NBTTagLongArray.toArray(p_i47525_1_));
	}
	
	private static long[] toArray(List<Long> p_193586_0_)
	{
		long[] along = new long[p_193586_0_.size()];
		int i = 0;
		while(i < p_193586_0_.size())
		{
			Long olong = p_193586_0_.get(i);
			along[i] = olong == null ? 0 : olong;
			++i;
		}
		return along;
	}
	
	@Override
	void write(DataOutput output) throws IOException
	{
		output.writeInt(this.data.length);
		long[] arrl = this.data;
		int n = arrl.length;
		int n2 = 0;
		while(n2 < n)
		{
			long i = arrl[n2];
			output.writeLong(i);
			++n2;
		}
	}
	
	@Override
	void read(DataInput input, int depth, NBTSizeTracker sizeTracker) throws IOException
	{
		sizeTracker.read(192);
		int i = input.readInt();
		sizeTracker.read(64 * i);
		this.data = new long[i];
		int j = 0;
		while(j < i)
		{
			this.data[j] = input.readLong();
			++j;
		}
	}
	
	@Override
	public byte getId()
	{
		return 12;
	}
	
	@Override
	public String toString()
	{
		StringBuilder stringbuilder = new StringBuilder("[L;");
		int i = 0;
		while(i < this.data.length)
		{
			if(i != 0)
			{
				stringbuilder.append(',');
			}
			stringbuilder.append(this.data[i]).append('L');
			++i;
		}
		return stringbuilder.append(']').toString();
	}
	
	@Override
	public NBTTagLongArray copy()
	{
		long[] along = new long[this.data.length];
		System.arraycopy(this.data, 0, along, 0, this.data.length);
		return new NBTTagLongArray(along);
	}
	
	@Override
	public boolean equals(Object p_equals_1_)
	{
		if(super.equals(p_equals_1_) && Arrays.equals(this.data, ((NBTTagLongArray) p_equals_1_).data))
		{
			return true;
		}
		return false;
	}
	
	@Override
	public int hashCode()
	{
		return super.hashCode() ^ Arrays.hashCode(this.data);
	}
}
