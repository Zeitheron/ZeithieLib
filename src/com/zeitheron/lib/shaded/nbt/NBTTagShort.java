package com.zeitheron.lib.shaded.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

@SuppressWarnings("serial")
public class NBTTagShort extends NBTPrimitive
{
	private short data;
	
	public NBTTagShort()
	{
	}
	
	public NBTTagShort(short data)
	{
		this.data = data;
	}
	
	@Override
	void write(DataOutput output) throws IOException
	{
		output.writeShort(this.data);
	}
	
	@Override
	void read(DataInput input, int depth, NBTSizeTracker sizeTracker) throws IOException
	{
		sizeTracker.read(80);
		this.data = input.readShort();
	}
	
	@Override
	public byte getId()
	{
		return 2;
	}
	
	@Override
	public String toString()
	{
		return String.valueOf(this.data) + "s";
	}
	
	@Override
	public NBTTagShort copy()
	{
		return new NBTTagShort(this.data);
	}
	
	@Override
	public boolean equals(Object p_equals_1_)
	{
		if(super.equals(p_equals_1_) && this.data == ((NBTTagShort) p_equals_1_).data)
		{
			return true;
		}
		return false;
	}
	
	@Override
	public int hashCode()
	{
		return super.hashCode() ^ this.data;
	}
	
	@Override
	public long getLong()
	{
		return this.data;
	}
	
	@Override
	public int getInt()
	{
		return this.data;
	}
	
	@Override
	public short getShort()
	{
		return this.data;
	}
	
	@Override
	public byte getByte()
	{
		return (byte) (this.data & 255);
	}
	
	@Override
	public double getDouble()
	{
		return this.data;
	}
	
	@Override
	public float getFloat()
	{
		return this.data;
	}
}
