package com.zeitheron.lib.shaded.nbt;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

public final class NBTUtil
{
	public static boolean areNBTEquals(NBTBase nbt1, NBTBase nbt2, boolean compareTagList)
	{
		if(nbt1 == nbt2)
		{
			return true;
		}
		if(nbt1 == null)
		{
			return true;
		}
		if(nbt2 == null)
		{
			return false;
		}
		if(!nbt1.getClass().equals(nbt2.getClass()))
		{
			return false;
		}
		if(nbt1 instanceof NBTTagCompound)
		{
			NBTTagCompound nbttagcompound = (NBTTagCompound) nbt1;
			NBTTagCompound nbttagcompound1 = (NBTTagCompound) nbt2;
			for(String s : nbttagcompound.getKeySet())
			{
				NBTBase nbtbase1 = nbttagcompound.getTag(s);
				if(NBTUtil.areNBTEquals(nbtbase1, nbttagcompound1.getTag(s), compareTagList))
					continue;
				return false;
			}
			return true;
		}
		if(nbt1 instanceof NBTTagList && compareTagList)
		{
			NBTTagList nbttaglist = (NBTTagList) nbt1;
			NBTTagList nbttaglist1 = (NBTTagList) nbt2;
			if(nbttaglist.hasNoTags())
			{
				return nbttaglist1.hasNoTags();
			}
			int i = 0;
			while(i < nbttaglist.tagCount())
			{
				NBTBase nbtbase = nbttaglist.get(i);
				boolean flag = false;
				int j = 0;
				while(j < nbttaglist1.tagCount())
				{
					if(NBTUtil.areNBTEquals(nbtbase, nbttaglist1.get(j), compareTagList))
					{
						flag = true;
						break;
					}
					++j;
				}
				if(!flag)
				{
					return false;
				}
				++i;
			}
			return true;
		}
		return nbt1.equals(nbt2);
	}
	
	public static NBTTagCompound read(ByteBuffer buf)
	{
		byte[] data = new byte[buf.getInt()];
		buf.get(data);
		try
		{
			return CompressedStreamTools.read(new DataInputStream(new ByteArrayInputStream(data)));
		} catch(IOException e)
		{
			e.printStackTrace();
			return new NBTTagCompound();
		}
	}
	
	public static void write(ByteBuffer buf, NBTTagCompound nbt)
	{
		ByteArrayOutputStream baos;
		baos = new ByteArrayOutputStream();
		Throwable throwable = null;
		try
		{
			DataOutputStream dos = new DataOutputStream(baos);
			try
			{
				CompressedStreamTools.write(nbt, dos);
			} finally
			{
				if(dos != null)
				{
					dos.close();
				}
			}
		} catch(Throwable throwable2)
		{
			throwable = throwable2;
			throw new Error(throwable);
		}
		buf.putInt(baos.size());
		buf.put(baos.toByteArray());
	}
}