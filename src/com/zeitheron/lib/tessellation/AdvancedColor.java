package com.zeitheron.lib.tessellation;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("serial")
public class AdvancedColor extends Color
{
	static final Map<Integer, AdvancedColor> cls = new HashMap<Integer, AdvancedColor>();
	
	public int color;
	
	public AdvancedColor(int rgb)
	{
		super(rgb);
		this.color = rgb;
		cls.put(rgb, this);
	}
	
	public AdvancedColor(int rgb, boolean hasalpha)
	{
		super(rgb, hasalpha);
		this.color = rgb;
		cls.put(rgb, this);
	}
	
	public static AdvancedColor of(int rgb)
	{
		AdvancedColor col = cls.get(rgb);
		if(col != null)
			return col;
		return new AdvancedColor(rgb);
	}
	
	public static AdvancedColor of(int rgb, boolean hasalpha)
	{
		AdvancedColor col = cls.get(rgb);
		if(col != null)
			return col;
		return new AdvancedColor(rgb, hasalpha);
	}
	
	public void setColor(int rgb)
	{
		this.color = rgb;
	}
	
	@Override
	public int getRGB()
	{
		return this.color;
	}
	
	@Override
	public int hashCode()
	{
		return this.color;
	}
}