package com.zeitheron.lib.tessellation;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Base64;

import javax.imageio.ImageIO;

public class ImageParser
{
	public static BufferedImage parseFromBase64(String base64String) throws IOException
	{
		return ImageIO.read(new ByteArrayInputStream(Base64.getMimeDecoder().decode(base64String)));
	}
}