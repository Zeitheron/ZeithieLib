package com.zeitheron.lib.tessellation;

import java.awt.Color;
import java.awt.Font;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class Matrix
{
	public final Map<TessellatorMode, Boolean> modes = new HashMap<TessellatorMode, Boolean>();
	public final LinkedList<Map<TessellatorMode, Boolean>> data = new LinkedList<>();
	Font font;
	Color color = new Color(0);
	int fontHeight;
	Tessellator tess;
	
	Matrix(Tessellator tess)
	{
		this.tess = tess;
	}
	
	public Matrix copy()
	{
		Matrix i = new Matrix(tess);
		i.font = this.font;
		i.color = this.color;
		i.fontHeight = this.fontHeight;
		return i;
	}
	
	void setColor(int rgb)
	{
		if(!this.getProp(TessellatorMode.BLEND))
			rgb = -16777216 | rgb;
		this.color = AdvancedColor.of(rgb, this.getProp(TessellatorMode.BLEND));
	}
	
	void setColor(Color color)
	{
		this.color = color;
	}
	
	public boolean getProp(TessellatorMode mode)
	{
		return this.modes.get(mode) == null ? false : this.modes.get(mode);
	}
	
	public void setProp(TessellatorMode mode, boolean bool)
	{
		this.modes.put(mode, bool);
	}
	
	public TessellationLastErrorState push()
	{
		data.push(new HashMap<>(modes));
		modes.clear();
		return TessellationLastErrorState.NONE;
	}
	
	public TessellationLastErrorState pop()
	{
		if(data.isEmpty())
			return TessellationLastErrorState.MATRIX_UNDERFLOW;
		modes.clear();
		modes.putAll(data.pop());
		return TessellationLastErrorState.NONE;
	}
}