package com.zeitheron.lib.tessellation;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JFrame;

@SuppressWarnings("serial")
public abstract class TessellatedWindow extends JFrame implements Runnable
{
	public static final Set<TessellatedWindow> ACTIVE_WINDOWS = new HashSet<TessellatedWindow>();
	public final Canvas canvas = new Canvas();
	protected Thread renderThread;
	protected boolean isRunning;
	protected int syncFPS = 60;
	protected int syncUPS = 20;
	protected int backgroundColor = 768955;
	private final BufferedImage screen;
	protected final Tessellator tessellator;
	
	protected void runBeforePacking()
	{
	}
	
	public TessellatedWindow(String title, int width, int height)
	{
		this.screen = new BufferedImage(width, height, 1);
		this.tessellator = new Tessellator(this.screen.createGraphics());
		this.setTitle(title);
		this.getContentPane().setPreferredSize(new Dimension(width, height));
		this.setResizable(false);
		this.setDefaultCloseOperation(3);
		this.add(this.canvas);
		this.runBeforePacking();
		this.pack();
		this.setLocationRelativeTo(null);
	}
	
	public void start()
	{
		this.isRunning = true;
		this.renderThread = new Thread(this);
		this.renderThread.setName("Tessellated Window " + this.getTitle());
		this.renderThread.start();
	}
	
	public void stop()
	{
		this.isRunning = false;
	}
	
	public void tick()
	{
	}
	
	public void render()
	{
	}
	
	protected void renderFrame()
	{
		BufferStrategy bs = this.canvas.getBufferStrategy();
		if(bs == null)
		{
			this.canvas.createBufferStrategy(2);
			this.canvas.requestFocus();
			return;
		}
		Color currCol = AdvancedColor.of(this.backgroundColor);
		Graphics g = bs.getDrawGraphics();
		Graphics scrg = this.screen.getGraphics();
		scrg.setColor(currCol);
		scrg.fillRect(0, 0, this.getWidth(), this.getHeight());
		this.tessellator.canvas = scrg;
		this.tessellator.setColor(Color.BLACK);
		this.tessellator.setFont(this.canvas.getFont());
		this.render();
		g.drawImage(this.screen, 0, 0, null);
		g.dispose();
		bs.show();
	}
	
	@Override
	public void run()
	{
		this.renderThread = Thread.currentThread();
		this.setVisible(true);
		int updLeft = 0;
		long lastUpd = System.currentTimeMillis();
		while(this.isRunning)
		{
			try
			{
				Thread.sleep(1000 / (long) this.syncFPS);
			} catch(Throwable err)
			{
				err.printStackTrace();
			}
			this.renderFrame();
			updLeft = (int) ((System.currentTimeMillis() - lastUpd) / (long) this.syncUPS);
			while(updLeft > 0)
			{
				lastUpd = System.currentTimeMillis();
				--updLeft;
				this.tick();
			}
		}
		this.setVisible(false);
	}
}
