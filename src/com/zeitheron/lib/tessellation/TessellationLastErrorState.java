package com.zeitheron.lib.tessellation;

public enum TessellationLastErrorState
{
	MATRIX_UNDERFLOW, NONE;
}