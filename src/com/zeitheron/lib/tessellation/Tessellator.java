package com.zeitheron.lib.tessellation;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Polygon;
import java.awt.image.ImageObserver;
import java.text.AttributedCharacterIterator;

public class Tessellator
{
	public Graphics canvas;
	private Matrix matrix = new Matrix(this);
	public TessellationLastErrorState state = TessellationLastErrorState.NONE;
	
	public Tessellator(Graphics canvas)
	{
		this.canvas = canvas;
	}
	
	public void pushMatrix()
	{
		this.state = matrix.push();
	}
	
	public void popMatrix()
	{
		this.state = matrix.pop();
	}
	
	public boolean tessIsEnabled(TessellatorMode mode)
	{
		return this.matrix.getProp(mode);
	}
	
	public void tessEnable(TessellatorMode mode)
	{
		this.matrix.setProp(mode, true);
	}
	
	public void tessDisable(TessellatorMode mode)
	{
		this.matrix.setProp(mode, false);
	}
	
	public void translate(double x, double y)
	{
		this.getDrawGraphics().translate(x, y);
	}
	
	public void setColor(int rgb)
	{
		this.matrix.setColor(rgb);
	}
	
	public void setColor(Color color)
	{
		this.matrix.setColor(color);
	}
	
	public void setFont(Font font)
	{
		this.matrix.font = font;
		if(font != null)
			this.matrix.fontHeight = getDrawGraphics().getFontMetrics(font).getHeight();
	}
	
	public Font getFont()
	{
		return this.matrix.font;
	}
	
	public Color getColor()
	{
		return this.matrix.color;
	}
	
	public int getIntColor()
	{
		return this.matrix.color.getRGB();
	}
	
	public void drawRectangle(int x, int y, int width, int height)
	{
		Graphics2D g = this.getDrawGraphics();
		g.setColor(this.matrix.color);
		g.setFont(this.matrix.font);
		g.drawRect(x, y, width, height);
	}
	
	public void fillRectangle(int x, int y, int width, int height)
	{
		Graphics2D g = this.getDrawGraphics();
		g.setColor(this.matrix.color);
		g.setFont(this.matrix.font);
		g.fillRect(x, y, width, height);
	}
	
	public void drawLine(int xPointA, int yPointA, int xPointB, int yPointB)
	{
		Graphics2D g = this.getDrawGraphics();
		g.setColor(this.matrix.color);
		g.setFont(this.matrix.font);
		g.drawLine(xPointA, yPointA, xPointB, yPointB);
	}
	
	public void drawImage(Image img, int x, int y, int width, int height, ImageObserver observer)
	{
		Graphics2D g = this.getDrawGraphics();
		g.setColor(this.matrix.color);
		g.setFont(this.matrix.font);
		g.drawImage(img, x, y, width, height, observer);
	}
	
	public void drawImage(Image img, int x, int y, ImageObserver observer)
	{
		Graphics2D g = this.getDrawGraphics();
		g.setColor(this.matrix.color);
		g.setFont(this.matrix.font);
		g.drawImage(img, x, y, observer);
	}
	
	public void drawImage(Image img, int dx1, int dy1, int dx2, int dy2, int sx1, int sy1, int sx2, int sy2, ImageObserver observer)
	{
		Graphics2D g = this.getDrawGraphics();
		g.setColor(this.matrix.color);
		g.setFont(this.matrix.font);
		g.drawImage(img, dx1, dy1, dx2, dy2, sx1, sy1, sx2, sy2, observer);
	}
	
	public void drawArc(int x, int y, int width, int height, int startAngle, int arcAngle)
	{
		Graphics2D g = this.getDrawGraphics();
		g.setColor(this.matrix.color);
		g.setFont(this.matrix.font);
		g.drawArc(x, y, width, height, startAngle, arcAngle);
	}
	
	public void drawOval(int x, int y, int width, int height)
	{
		Graphics2D g = this.getDrawGraphics();
		g.setColor(this.matrix.color);
		g.setFont(this.matrix.font);
		g.drawOval(x, y, width, height);
	}
	
	public void drawPolygon(Polygon polygon)
	{
		Graphics2D g = this.getDrawGraphics();
		g.setColor(this.matrix.color);
		g.setFont(this.matrix.font);
		g.drawPolygon(polygon);
	}
	
	public void drawPolyline(int[] xPoints, int[] yPoints, int nPoints)
	{
		Graphics2D g = this.getDrawGraphics();
		g.setColor(this.matrix.color);
		g.setFont(this.matrix.font);
		g.drawPolyline(xPoints, yPoints, nPoints);
	}
	
	public void drawRoundRectangle(int x, int y, int width, int height, int arcWidth, int arcHeight)
	{
		Graphics2D g = this.getDrawGraphics();
		g.setColor(this.matrix.color);
		g.setFont(this.matrix.font);
		g.drawRoundRect(x, y, width, height, arcWidth, arcHeight);
	}
	
	public void drawString(String string, int x, int y)
	{
		Graphics2D g = this.getDrawGraphics();
		g.setColor(this.matrix.color);
		g.setFont(this.matrix.font);
		g.drawString(string, x, y + this.matrix.fontHeight);
	}
	
	public void drawString(AttributedCharacterIterator iterator, int x, int y)
	{
		Graphics2D g = this.getDrawGraphics();
		g.setColor(this.matrix.color);
		g.setFont(this.matrix.font);
		g.drawString(iterator, x, y + this.matrix.fontHeight);
	}
	
	public Graphics2D getDrawGraphics()
	{
		return this.canvas instanceof Graphics2D ? (Graphics2D) this.canvas : null;
	}
	
	public void checkError() throws TessellatorException
	{
		if(this.state != TessellationLastErrorState.NONE && this.state != null)
		{
			throw new TessellatorException(this.state.name());
		}
	}
}
