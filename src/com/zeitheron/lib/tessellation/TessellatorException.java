package com.zeitheron.lib.tessellation;

@SuppressWarnings("serial")
public class TessellatorException extends Exception
{
	public TessellatorException(String cause)
	{
		super(cause);
	}
}