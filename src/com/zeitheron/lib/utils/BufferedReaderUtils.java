/* Decompiled with CFR 0_123. */
package com.zeitheron.lib.utils;

import java.io.BufferedReader;

import com.zeitheron.lib.utils.iIntent;

public class BufferedReaderUtils
{
	public static void forEachLine(BufferedReader reader, iIntent intent)
	{
		try
		{
			String ln = reader.readLine();
			while(ln != null)
			{
				intent.run(0, ln);
				ln = reader.readLine();
			}
		} catch(Throwable ln)
		{
			// empty catch block
		}
	}
}
