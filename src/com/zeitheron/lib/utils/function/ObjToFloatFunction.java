package com.zeitheron.lib.utils.function;

@FunctionalInterface
public interface ObjToFloatFunction<T>
{
	float applyAsFloat(T i);
}