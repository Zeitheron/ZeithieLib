package com.zeitheron.lib.utils.function;

@FunctionalInterface
public interface ObjToLongFunction<T>
{
	long applyAsLong(T i);
}