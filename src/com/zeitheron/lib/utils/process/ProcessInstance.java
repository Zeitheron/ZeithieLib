package com.zeitheron.lib.utils.process;

public class ProcessInstance
{
	private String pid;
	private String name;
	
	public ProcessInstance()
	{
	}
	
	public String getPid()
	{
		return this.pid;
	}
	
	public void setPid(String pid)
	{
		this.pid = pid;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
}