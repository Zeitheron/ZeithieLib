package com.zeitheron.lib.utils.process;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.zeitheron.lib.annotations.MayBeNull;

public class ProcessManager
{
	public static ProcessInstance currentProcess()
	{
		return getProcessById(getCurrentPid());
	}
	
	@MayBeNull
	public static ProcessInstance getProcessById(String pid)
	{
		for(ProcessInstance inst : getProcesses())
			if(inst.getPid().equals(pid))
				return inst;
		return null;
	}
	
	public static String getCurrentPid()
	{
		return java.lang.management.ManagementFactory.getRuntimeMXBean().getName().split("@")[0];
	}
	
	public static List<ProcessInstance> getProcesses()
	{
		try
		{
			Process process = Runtime.getRuntime().exec("tasklist /fo csv");
			List<ProcessInstance> entities = new ArrayList<>();
			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			Throwable localThrowable3 = null;
			try
			{
				String line;
				String[] split;
				while((line = reader.readLine()) != null)
				{
					split = line.split(",");
					ProcessInstance entity = new ProcessInstance();
					entity.setName(split[0].replaceAll("\"", ""));
					entity.setPid(split[1].replaceAll("\"", ""));
					entities.add(entity);
				}
				process.destroyForcibly();
				return entities;
			} catch(Throwable localThrowable1)
			{
				localThrowable3 = localThrowable1;
				throw localThrowable1;
			} finally
			{
				if(reader != null)
				{
					if(localThrowable3 != null)
					{
						try
						{
							reader.close();
						} catch(Throwable localThrowable2)
						{
							localThrowable3.addSuppressed(localThrowable2);
						}
					} else
					{
						reader.close();
					}
				}
			}
		} catch(IOException e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static void terminateProcess(ProcessInstance entity)
	{
		try
		{
			Runtime.getRuntime().exec("taskkill /f /pid " + entity.getPid());
		} catch(IOException e)
		{
			e.printStackTrace();
		}
	}
}